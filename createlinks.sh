ln -nfs ".zprezto_overrides/zshrc"     ".zshrc"
ln -nfs ".zprezto_overrides/zpreztorc" ".zpreztorc"
ln -nfs ".zprezto_overrides/inputrc"   ".inputrc"
ln -nfs ".zprezto_overrides/editrc"    ".editrc"

ln -nfs ".zprezto/runcoms/zlogin"      ".zlogin"
ln -nfs ".zprezto/runcoms/zlogout"     ".zlogout"
ln -nfs ".zprezto/runcoms/zprofile"    ".zprofile"
ln -nfs ".zprezto/runcoms/zshenv"      ".zshenv"

ln -nfs ".zprezto_overrides/p10k.zsh"     ".p10k.zsh"

# cygwin
# ln -nfs ".zprezto_overrides/zlogin"    ".zlogin"
# ln -nfs ".zprezto_overrides/zlogout"   ".zlogout"
