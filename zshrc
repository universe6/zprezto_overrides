
#########################################################################
# ln -nfs ".vim/vimrc"                   ".vimrc"
# ln -nfs ".vim/gvimrc"                  ".gvimrc"
# ln -nfs ".zprezto_overrides/zshrc"     ".zshrc"
# ln -nfs ".zprezto_overrides/zpreztorc" ".zpreztorc"
# ln -nfs ".zprezto_overrides/inputrc"   ".inputrc"
# ln -nfs ".zprezto_overrides/editrc"    ".editrc"
#
# ln -nfs ".zprezto/runcoms/zlogin"      ".zlogin"
# ln -nfs ".zprezto/runcoms/zlogout"     ".zlogout"
# ln -nfs ".zprezto/runcoms/zprofile"    ".zprofile"
# ln -nfs ".zprezto/runcoms/zshenv"      ".zshenv"
#
# cygwin
# ln -nfs ".zprezto_overrides/zlogin"    ".zlogin"
# ln -nfs ".zprezto_overrides/zlogout"   ".zlogout"
#
#########################################################################

#
# Executes commands at the start of an interactive session.
#
# Authors:
#   Sorin Ionescu <sorin.ionescu@gmail.com>
#

if [ $TILIX_ID ] || [ $VTE_VERSION ]; then
        source /etc/profile.d/vte-2.91.sh
fi

# Source Prezto.
if [[ -s "${ZDOTDIR:-$HOME}/.zprezto/init.zsh" ]]; then
  source "${ZDOTDIR:-$HOME}/.zprezto/init.zsh"
fi

zsh ~/.vim.local/plugged/vim-solarized8/scripts/solarized8.sh


export RIPGREP_CONFIG_PATH=$HOME/.ripgreprc

# Customize to your needs...

# aliases taken from yadr repo

# Get operating system
platform='unknown'
unamestr=$(uname)
if [[ $unamestr == 'Linux' ]]; then
  platform='linux'
elif [[ $unamestr == 'Darwin' ]]; then
  platform='darwin'
fi

# PS
alias psa="ps aux"
alias psg="ps aux | grep "
alias psr='ps aux | grep ruby'

# Moving around
alias cdb='cd -'
alias cls='clear;ls'

# Show human friendly numbers and colors
alias df='df -h'
alias du='du -h -d 2'

if [[ $platform == 'linux' ]]; then
#  alias ll='ls -alh --color=auto'
  alias ll='ls -alhF'
  alias ls='ls -F'
elif [[ $platform == 'darwin' ]]; then
  alias ll='ls -alGh'
  alias ls='ls -Gh'
fi

# show me files matching "ls grep"
alias lsg='ll | grep'


alias gtop='cd "$(git rev-parse --show-toplevel)"'

alias gar="killall -HUP -u \"$USER\" zsh"  #global alias reload

# vimrc editing
alias ve='vim ~/.vimrc'

# zsh profile editing
alias ze='vim ~/.zshrc'

# Common shell functions
alias less='less -r'
alias tf='tail -f'
alias l='less'
alias lh='ls -altF | head' # see the last modified files
alias screen='TERM=screen screen'
alias cl='clear'


alias ka9='killall -9'
alias k9='kill -9'

# Homebrew
alias brewu='brew update  && brew upgrade && brew cleanup && brew prune && brew doctor'

#yadr end

alias fv='fasd -fe vim'

alias a='fasd -a'        # any
alias s='fasd -si'       # show / search / select
alias d='fasd -d'        # directory
alias f='fasd -f'        # file
alias sd='fasd -sid'     # interactive directory selection
alias sf='fasd -sif'     # interactive file selection
alias z='fasd_cd -d'     # cd, same functionality as j in autojump
alias zz='fasd_cd -d -i' # cd with interactive selection

path+=~/bin
path+=/usr/sbin
path+=`yarn global bin --quiet >/dev/null 2>&1`

# Uncomment following line if you want red dots to be displayed while waiting for completion
COMPLETION_WAITING_DOTS="true"

HISTSIZE=50000                   # The maximum number of events to save in the internal history.
SAVEHIST=50000                   # The maximum number of events to save in the history file.

# User specific aliases and functions

alias 11='dirs -v'

alias h='history 1'
alias rm='rm -i'
alias cp='cp -i'
alias mv='mv -i'

# Misc :)
alias less='less -r'                          # raw control characters
alias whence='type -a'                        # where, of a sort
alias grep='grep --color'                     # show differences in colour
alias egrep='egrep --color=auto'              # show differences in colour
alias fgrep='fgrep --color=auto'              # show differences in colour
#
# Some shortcuts for different directory listings
alias ll='ls -lhF'                              # long list
alias la='ls -lAFh'                              # all but . and ..
alias l='ls -CF'                              #

# prompt customization
# PROMPT='%m '${PROMPT}

alias ag='ag -U'
alias agg='ag -U -g'

unsetopt EXTENDED_HISTORY          # DO NOT Write the history file in the ':start:elapsed;command' format.
  setopt HIST_BEEP                 # Beep when accessing non-existent history.
  setopt APPEND_HISTORY            # Allow multiple terminal sessions to all append to one zsh command history
  setopt HIST_REDUCE_BLANKS        # Remove extra blanks from each command line being added to history

unsetopt correct
unsetopt correct_all


# CASE_SENSITIVE="true"
# KEYTIMEOUT=1

bindkey -M vicmd '?' vi-history-search-forward
bindkey -M vicmd '/' vi-history-search-backward

#autoload -U edit-command-line
#zle -N edit-command-line
#bindkey '^E' edit-command-line                   # Opens Vim to edit current command line
#bindkey '^R' history-incremental-search-backward # Perform backward search in command line history
#bindkey '^S' history-incremental-search-forward  # Perform forward search in command line history
#bindkey '^P' history-search-backward             # Go back/search in history (autocomplete)
#bindkey '^N' history-search-forward              # Go forward/search in history (autocomplete)


#bindkey '\e[A' history-substring-search-up
#bindkey '\e[B' history-substring-search-down
bindkey '\eOA' history-substring-search-up
bindkey '\eOB' history-substring-search-down


















# set special keys so they work in VIM mode as well

#autoload zkbd
#[[ ! -f ${ZDOTDIR:-$HOME}/.zkbd/$TERM-${DISPLAY:-$VENDOR-$OSTYPE} ]] && zkbd
#source ${ZDOTDIR:-$HOME}/.zkbd/$TERM-${DISPLAY:-$VENDOR-$OSTYPE}
#
#[[ -n ${key[Backspace]} ]] && bindkey "${key[Backspace]}" backward-delete-char
#[[ -n ${key[Insert]} ]] && bindkey "${key[Insert]}" overwrite-mode
#[[ -n ${key[Home]} ]] && bindkey "${key[Home]}" beginning-of-line
#[[ -n ${key[PageUp]} ]] && bindkey "${key[PageUp]}" up-line-or-history
#[[ -n ${key[Delete]} ]] && bindkey "${key[Delete]}" delete-char
#[[ -n ${key[End]} ]] && bindkey "${key[End]}" end-of-line
#[[ -n ${key[PageDown]} ]] && bindkey "${key[PageDown]}" down-line-or-history
#[[ -n ${key[Up]} ]] && bindkey "${key[Up]}" up-line-or-search
#[[ -n ${key[Left]} ]] && bindkey "${key[Left]}" backward-char
#[[ -n ${key[Down]} ]] && bindkey "${key[Down]}" down-line-or-search
#[[ -n ${key[Right]} ]] && bindkey "${key[Right]}" forward-char

# To customize prompt, run `p10k configure` or edit ~/.p10k.zsh.
[[ -f ~/.p10k.zsh ]] && source ~/.p10k.zsh
